$('.laliste').hide();
(function() {
  'use strict';

  var section = document.querySelectorAll(".menuScroll");
  var sections = {};
  var i = 0;

  Array.prototype.forEach.call(section, function(e) {
    sections[e.id] = e.offsetTop;
  });

  	window.onscroll = function() {
	    var scrollPosition = document.documentElement.scrollTop || document.body.scrollTop;
	    for (i in sections) {
	      if (sections[i] < scrollPosition) {
	        $('.laliste').show();
	      }else{
	      	$('.laliste').hide();
	      }
	    }
	    $(".croix").click(function(){
	        $('.laliste').hide();
	    });
  	};
})();
